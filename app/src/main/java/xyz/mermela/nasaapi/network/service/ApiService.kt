package xyz.mermela.nasaapi.network.service


import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import xyz.mermela.nasaapi.model.ServiceResponse

interface ApiService {

    @GET("mars-photos/api/v1/rovers/curiosity/photos")
    fun getImageCuriosity(
            @Query("api_key") api_key : String,
            @Query("sol") sol : Int,
    ) : Call<ServiceResponse>

    @GET("mars-photos/api/v1/rovers/curiosity/photos")
    fun getImageCuriosityByCamera(
            @Query("api_key") api_key : String,
            @Query("sol") sol : Int,
            @Query("camera") camera : String
    ): Call<ServiceResponse>




    @GET("mars-photos/api/v1/rovers/opportunity/photos")
    fun getImageOpportunity(
            @Query("api_key") api_key : String,
            @Query("sol") sol : Int
    ) : Call<ServiceResponse>

    @GET("mars-photos/api/v1/rovers/opportunity/photos")
    fun getImageOpportunityByCamera(
            @Query("api_key") api_key : String,
            @Query("sol") sol : Int,
            @Query("camera") camera : String
    ) : Call<ServiceResponse>




    @GET("mars-photos/api/v1/rovers/spirit/photos")
    fun getImageSpirit(
            @Query("api_key") api_key : String,
            @Query("sol") sol : Int
    ) : Call<ServiceResponse>

    @GET("mars-photos/api/v1/rovers/spirit/photos")
    fun getImageSpiritByCamera(
            @Query("api_key") api_key : String,
            @Query("sol") sol : Int,
            @Query("camera") camera : String
    ) : Call<ServiceResponse>



}