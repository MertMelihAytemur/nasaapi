package xyz.mermela.nasaapi.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import xyz.mermela.nasaapi.BuildConfig
import xyz.mermela.nasaapi.network.service.ApiService
import java.util.concurrent.TimeUnit

class NetworkHelper {
    var apiService : ApiService? = null

    init{
        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.nasa.gov/")
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        apiService =  retrofit.create(ApiService::class.java)
    }


    private fun getClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.addInterceptor(createHttpLoggingInterceptor(BuildConfig.DEBUG))
        return httpClient.build()
    }

    private fun createHttpLoggingInterceptor(debugMode: Boolean): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        if (debugMode) httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        else httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE

        return httpLoggingInterceptor
    }
}