package xyz.mermela.nasaapi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_opportunity.view.*
import xyz.mermela.nasaapi.R
import xyz.mermela.nasaapi.`interface`.OnAppcentClickListener
import xyz.mermela.nasaapi.model.Photo

class OpportunityRecyclerAdapter(
    private val context : Context,
    private val photoList : List<Photo>,
    var appcentClickListener: OnAppcentClickListener

    ) : RecyclerView.Adapter<OpportunityRecyclerAdapter.ViewHolder>(){

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OpportunityRecyclerAdapter.ViewHolder {
            val inflatedView = LayoutInflater.from(context).inflate(R.layout.item_opportunity,parent,false)
            return ViewHolder(inflatedView)

        }

        override fun onBindViewHolder(holder: OpportunityRecyclerAdapter.ViewHolder, position: Int) {
            holder.bind()
        }

        override fun getItemCount(): Int = photoList.size

        inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
            fun bind(){
                val item = photoList[adapterPosition]
                Picasso.get().load(item.img_src).into(itemView.ivOpportunity)
                itemView.setOnClickListener {
                    appcentClickListener.onClick(adapterPosition)
                }

            }


        }
}