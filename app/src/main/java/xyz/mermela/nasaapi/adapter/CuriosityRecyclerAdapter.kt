package xyz.mermela.nasaapi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_curiosity.view.*
import xyz.mermela.nasaapi.R
import xyz.mermela.nasaapi.`interface`.OnAppcentClickListener
import xyz.mermela.nasaapi.model.Photo


class CuriosityRecyclerAdapter(
        private val context : Context,
        private val photoList : List<Photo>,
        var appcentClickListener: OnAppcentClickListener

        ) : RecyclerView.Adapter<CuriosityRecyclerAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CuriosityRecyclerAdapter.ViewHolder {
        val inflatedView = LayoutInflater.from(context).inflate(R.layout.item_curiosity,parent,false)
        return ViewHolder(inflatedView)

    }

    override fun onBindViewHolder(holder: CuriosityRecyclerAdapter.ViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = photoList.size

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bind(){
        val item = photoList[adapterPosition]
            Picasso.get().load(item.img_src).into(itemView.ivCuriosity)
            itemView.setOnClickListener {
                appcentClickListener.onClick(adapterPosition)
            }



        }


    }
}