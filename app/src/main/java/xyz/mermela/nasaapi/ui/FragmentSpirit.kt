package xyz.mermela.nasaapi.ui

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.provider.SyncStateContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_opportunity.*
import kotlinx.android.synthetic.main.fragment_spirit.*
import kotlinx.android.synthetic.main.photo_item_info.view.*
import retrofit2.Call
import retrofit2.Response
import xyz.mermela.nasaapi.R
import xyz.mermela.nasaapi.`interface`.OnAppcentClickListener
import xyz.mermela.nasaapi.adapter.CuriosityRecyclerAdapter
import xyz.mermela.nasaapi.adapter.SpiritRecyclerAdapter
import xyz.mermela.nasaapi.model.Photo
import xyz.mermela.nasaapi.model.ServiceResponse
import xyz.mermela.nasaapi.network.NetworkHelper
import xyz.mermela.nasaapi.utils.Constants
import javax.security.auth.callback.Callback

class FragmentSpirit : Fragment() {
    var progressDialog : ProgressDialog? = null
    val networkHelper = NetworkHelper()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_spirit,container,false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        progressDialog = activity?.let { ProgressDialog(it) }

        getImageSpirit() //  Default.
        showPopUpMenuSpirit.setOnClickListener {
            spiritCameraChooser()
        }

    }

    private fun getImageSpirit(){
        showProgress(true)
        networkHelper.apiService?.getImageSpirit(Constants.apikey,999)
                ?.enqueue(object : retrofit2.Callback<ServiceResponse>{
                    override fun onResponse(call: Call<ServiceResponse>, response: Response<ServiceResponse>) {
                        showProgress(false)
                        response.body()?.photos?.let {
                            response.body()?.photos?.let { it1->
                                setRecyclerViewAdapter(it1)
                            }
                        }
                    }
                    override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                        showProgress(false)
                        Toast.makeText(activity, "Sorgu Başarısız !", Toast.LENGTH_SHORT).show()
                    }

                })

    }
    private fun getImageSpiritByCamera(cameraName : String){
        showProgress(true)
        networkHelper.apiService?.getImageSpiritByCamera(Constants.apikey,999,cameraName)
                ?.enqueue(object : retrofit2.Callback<ServiceResponse>{
                    override fun onResponse(call: Call<ServiceResponse>, response: Response<ServiceResponse>) {
                        showProgress(false)
                        response.body()?.photos?.let {
                            response.body()?.photos?.let { it1->
                                setRecyclerViewAdapter(it1)
                            }
                        }
                    }
                    override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                        showProgress(false)
                        Toast.makeText(activity, "Sorgu Başarısız !", Toast.LENGTH_SHORT).show()
                    }

                })

    }


    private fun setRecyclerViewAdapter(photoList : List<Photo>){
        val mLayoutManager = GridLayoutManager(context,3)
        spiritRecyclerView.layoutManager = mLayoutManager
        spiritRecyclerView.adapter = context?.let {
            SpiritRecyclerAdapter(it, photoList, object : OnAppcentClickListener {
                @SuppressLint("SetTextI18n")
                override fun onClick(position: Int) {
                    showDetail(position,photoList)
                }
            })
        }

    }

    private fun showProgress(show : Boolean){
        if(show){
            progressDialog?.show()
        }else
            progressDialog?.dismiss()

    }

    @SuppressLint("SetTextI18n")
    private fun showDetail(data : Int, photoList: List<Photo>){

        val datas = photoList[data]
        val myDailogView = LayoutInflater.from(context).inflate(R.layout.photo_item_info, null)
        val builder = AlertDialog.Builder(context)
                .setView(myDailogView)
                .setTitle("INFO")
        builder.show()

        Picasso.get().load(datas.img_src).into(myDailogView.ivRoverImage)
        myDailogView.tvEarthDate.text = "-Çekildiği Tarih : " + datas.earth_date
        myDailogView.tvRoverName.text = "-Araç Adı : " + datas.rover.name
        myDailogView.tvRoverCameraName.text = "-Kamera Adı : "+datas.camera.name
        myDailogView.tvtvRoverStatus.text = "-Görev Durumu : " + datas.rover.status
        myDailogView.tvLandingDate.text = "-İniş Tarihi : " + datas.rover.landing_date
        myDailogView.tvLaunchDate.text = "-Fırlatılma Tarihi : " + datas.rover.launch_date

    }

    private fun spiritCameraChooser(){
        val popup = PopupMenu(context, showPopUpMenuSpirit)
        popup.menuInflater.inflate(R.menu.menu_spirit, popup.menu)

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.all -> {
                    getImageSpirit()
                    showPopUpMenuSpirit.setText("All")
                    true
                }
                R.id.fhaz -> {
                    getImageSpiritByCamera("fhaz")
                    showPopUpMenuSpirit.setText("FHAZ")
                    true
                }
                R.id.rhaz -> {
                    getImageSpiritByCamera("rhaz")
                    showPopUpMenuSpirit.setText("RHAZ")
                    true
                }

                R.id.navcam -> {
                    getImageSpiritByCamera("navcam")
                    showPopUpMenuSpirit.setText("NAVCAM")
                    true
                }
                R.id.pancam -> {
                    getImageSpiritByCamera("pancam")
                    showPopUpMenuSpirit.setText("PANCAM")
                    true
                }
                R.id.minites -> {
                    getImageSpiritByCamera("mınıtes")
                    showPopUpMenuSpirit.setText("MINITES")
                    true
                }

                else -> {


                    false
                }

            }
        }
        popup.show()
    }

}