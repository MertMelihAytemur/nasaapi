package xyz.mermela.nasaapi.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*
import xyz.mermela.nasaapi.R

class MainActivity : AppCompatActivity() {

    private val fragmentList = ArrayList<Fragment>()
    private val tabLayoutTitles = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        fragmentList.add(FragmentCuriosity())
        fragmentList.add(FragmentOpportunity())
        fragmentList.add(FragmentSpirit())

        val adapter = MyViewPagerAdapter(this)
        viewPager.adapter = adapter

        tabLayoutTitles.add("CURIOSITY")
        tabLayoutTitles.add("OPPORTUNITY")
        tabLayoutTitles.add("SPIRIT")

        TabLayoutMediator(tabLayout,viewPager){tab,position->
            tab.text = tabLayoutTitles[position]

        }.attach()

    }

    inner class MyViewPagerAdapter(fragmentActivity: FragmentActivity) :
        FragmentStateAdapter(fragmentActivity) {
        override fun getItemCount(): Int {
            return fragmentList.size
        }

        override fun createFragment(position: Int): Fragment {
            return fragmentList[position]
        }

    }




}