package xyz.mermela.nasaapi.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import xyz.mermela.nasaapi.R

class ProgressDialog(context: Context) : Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_prosess)
    }
}