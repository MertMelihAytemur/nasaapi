package xyz.mermela.nasaapi.ui

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_curiosity.*
import kotlinx.android.synthetic.main.item_curiosity.*
import kotlinx.android.synthetic.main.item_curiosity.view.*
import kotlinx.android.synthetic.main.photo_item_info.*
import kotlinx.android.synthetic.main.photo_item_info.view.*
import retrofit2.Call
import retrofit2.Response
import xyz.mermela.nasaapi.R
import xyz.mermela.nasaapi.`interface`.OnAppcentClickListener
import xyz.mermela.nasaapi.adapter.CuriosityRecyclerAdapter
import xyz.mermela.nasaapi.model.Photo
import xyz.mermela.nasaapi.model.ServiceResponse
import xyz.mermela.nasaapi.network.NetworkHelper
import xyz.mermela.nasaapi.utils.Constants


class FragmentCuriosity : Fragment() {
    private var progressDialog: ProgressDialog? = null
    var networkHelper = NetworkHelper()


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_curiosity, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        progressDialog = activity?.let { ProgressDialog(it) }

        getImageCuriosity() // Default.
        showPopUpMenuCuriosity.setOnClickListener {
            curiosityCameraChooser()
        }


    }

    private fun getImageCuriosity() {
        showProgress(true)
        networkHelper.apiService?.getImageCuriosity(Constants.apikey, sol = 1000)
                ?.enqueue(object : retrofit2.Callback<ServiceResponse> {
                    override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                        showProgress(false)
                        Toast.makeText(activity, "Sorgu Başarısız !", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(
                            call: Call<ServiceResponse>,
                            response: Response<ServiceResponse>
                    ) {
                        showProgress(false)
                        response.body()?.photos?.let {
                            response.body()?.photos?.let { it1 ->
                                setRecyclerViewAdapter(
                                        it1
                                )
                            }
                        }
                    }

                })
    }

    private fun getImageCuriosityByCamera(cameraName: String) {
        showProgress(true)
        networkHelper.apiService?.getImageCuriosityByCamera(Constants.apikey, sol = 1000, cameraName)
                ?.enqueue(object : retrofit2.Callback<ServiceResponse> {
                    override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                        showProgress(false)
                        Toast.makeText(activity, "Sorgu Başarısız !", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(
                            call: Call<ServiceResponse>,
                            response: Response<ServiceResponse>
                    ) {
                        showProgress(false)
                        response.body()?.photos?.let {
                            response.body()?.photos?.let { it1 ->
                                setRecyclerViewAdapter(
                                        it1
                                )
                            }
                        }
                    }

                })
    }


    private fun setRecyclerViewAdapter(photoList: List<Photo>) {
        val mLayoutManager = GridLayoutManager(context, 3)
        curiosityRecyclerView.layoutManager = mLayoutManager
        curiosityRecyclerView.adapter = context?.let {
            CuriosityRecyclerAdapter(it, photoList, object : OnAppcentClickListener {
                @SuppressLint("SetTextI18n")
                override fun onClick(position: Int) {
                    showDetail(position, photoList)
                }


            })
        }


    }

    private fun showProgress(show: Boolean) {
        if (show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    @SuppressLint("SetTextI18n")          //-------------------Show photo details.
    private fun showDetail(position: Int, photoList: List<Photo>) {

        val datas = photoList[position]
        val myDailogView = LayoutInflater.from(context).inflate(R.layout.photo_item_info, null)
        val builder = AlertDialog.Builder(context)
                .setView(myDailogView)
                .setTitle("INFO")
        builder.show()

        Picasso.get().load(datas.img_src).into(myDailogView.ivRoverImage)
        myDailogView.tvEarthDate.text = "-Çekildiği Tarih : " + datas.earth_date
        myDailogView.tvRoverName.text = "-Araç Adı : " + datas.rover.name
        myDailogView.tvRoverCameraName.text = "-Kamera Adı : " + datas.camera.name
        myDailogView.tvtvRoverStatus.text = "-Görev Durumu : " + datas.rover.status
        myDailogView.tvLandingDate.text = "-İniş Tarihi : " + datas.rover.landing_date
        myDailogView.tvLaunchDate.text = "-Fırlatılma Tarihi : " + datas.rover.launch_date

    }

    private fun curiosityCameraChooser(){
        val popup = PopupMenu(context, showPopUpMenuCuriosity)
        popup.menuInflater.inflate(R.menu.menu_curiosity, popup.menu)

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.all -> {
                    getImageCuriosity()
                    showPopUpMenuCuriosity.setText("All")
                    true
                }
                R.id.fhaz -> {
                    getImageCuriosityByCamera("fhaz")
                    showPopUpMenuCuriosity.setText("FHAZ")
                    true
                }
                R.id.rhaz -> {
                    getImageCuriosityByCamera("rhaz")
                    showPopUpMenuCuriosity.setText("RHAZ")
                    true
                }
                R.id.mast -> {
                    getImageCuriosityByCamera("mast")
                    showPopUpMenuCuriosity.setText("MAST")
                    true
                }
                R.id.chemcam -> {
                    getImageCuriosityByCamera("chemcam")
                    showPopUpMenuCuriosity.setText("CHEMCAM")
                    true
                }
                R.id.mahli -> {
                    getImageCuriosityByCamera("mahlı")
                    showPopUpMenuCuriosity.setText("MAHLI")
                    true
                }
                R.id.mardi -> {
                    getImageCuriosityByCamera("mardı")
                    showPopUpMenuCuriosity.setText("MARDI")
                    true
                }
                R.id.navcam -> {
                    getImageCuriosityByCamera("navcam")
                    showPopUpMenuCuriosity.setText("NAVCAM")
                    true
                }

                else -> {
                    false
                }

            }
        }
        popup.show()
    }


}