package xyz.mermela.nasaapi.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


data class ServiceResponse(
    val photos: List<Photo>
)

